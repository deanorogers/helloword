package com.dean.services;


import com.dean.domain.Greeting;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/service")
public class HelloWorldController {

    @RequestMapping("/greeting")
    @ResponseBody
    public Greeting getGreeting () {
        Greeting greeting = new Greeting("Hello");
        return greeting;
    }

}
